<?php
/**
 * [PHPFOX_HEADER]
 */

defined('PHPFOX') or exit('NO DICE!');

/**
 * Profile Block Header
 *
 * @copyright		[PHPFOX_COPYRIGHT]
 * @author  		Raymond Benc
 * @package  		Module_Profile
 * @version 		$Id: logo.class.php 4963 2012-10-29 07:33:30Z Raymond_Benc $
 */
class Gigamobile_Component_Block_Login extends Phpfox_Component
{
    /**
     * Class process method wnich is used to execute this component.
     */
    public function process()
    {
        $aBackground = Phpfox::getService('mobileaddon.background')->getActive();
        //vdd($aBackground);
        $sImage = sprintf($aBackground['image'], '');
/*        $sValue = Phpfox::getLib('image.helper')->display(array(
                'path' => 'mobileaddon.image_url',
                'file' => $aBackground['image'],
                'max_width' => 400,
                'max_height' => 400
            )
        );*/
       // vdd(Phpfox::getParam('mobileaddon.image_url').$sImage);
       // vdd($this->url()->makeUrl('',$sImage));

        $sHeader = Phpfox::getLib('template')->getHeader();

        $this->template()
            ->assign('
				<style type="text/css">#holder { background:url(' .Phpfox::getParam('mobileaddon.image_url').$sImage. '); margin:auto; text-align:center; }</style>
			' . $sHeader)
            ->assign(array(
                'aImages' => Phpfox::getParam('mobileaddon.image_url').$sImage,
                'sImage' => $sImage
            ));
    }

    /**
     * Garbage collector. Is executed after this class has completed
     * its job and the template has also been displayed.
     */
    public function clean()
    {
        (($sPlugin = Phpfox_Plugin::get('profile.component_block_logo_clean')) ? eval($sPlugin) : false);
    }
}

?>