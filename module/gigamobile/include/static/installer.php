<?php
if (!class_exists('GigamobileInstaller')) {
    class GigamobileInstaller
    {
        var $sVersion;

        function __construct($sVersion) {
            $this->sVersion = $sVersion;
        }

        var $bNoInstall = false;

        function install()
        {
            if ($this->bNoInstall) {
                return false;
            }

            $oDb = Phpfox::getLib('phpfox.database');
            $aSql = array(
                "DROP TABLE IF EXISTS `" . Phpfox::getT('gigadev_mobileaddon_background') . "`",
                "CREATE TABLE `" . Phpfox::getT('gigadev_mobileaddon_background') . "` (
                     `id` INT NOT NULL AUTO_INCREMENT PRIMARY KEY ,
                     `title` TEXT CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL ,
                     `image` TEXT NOT NULL,
                      `active` BOOLEAN NOT NULL,
                      `time_stamp` INT NOT NULL
                )"
            );
            foreach ($aSql as $sSql) {
                $oDb->query($sSql);
            }
        }

        function upgrade()
        {

        }

        function uninstall()
        {
            $oDb = Phpfox::getLib('phpfox.database');
            $aSql = array(
                'DROP TABLE IF EXISTS `' . Phpfox::getT('gigadev_mobileaddon_background') . '`',
            );
            foreach ($aSql as $sSql) {
                $oDb->query($sSql);
            }
        }
    }
}

$oRequest = Phpfox::getLib('phpfox.request');

if ($oRequest->get('upgrade')) {
    $sType = 'upgrade';
} else if ($oRequest->get('delete')) {
    $sType = 'uninstall';
} else if ($oRequest->get('install')) {
    $sType = 'install';
}

$oInstaller = new GigamobileInstaller($sVersion);
$oInstaller->{$sType}();