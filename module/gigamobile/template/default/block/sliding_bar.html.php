{if Phpfox::isUser()}
<div id="slidingbar_top">
<div class="slidingbar_user_img">{img user=$sUser suffix='_75_square' max_width=75 max_height=75}</div>
<div class="slidingbar_user_name"><a href="{url link=''$sUser.user_name'}">{$sUser.full_name|shorten:15:'..'}</a></div>
</div>
{/if}

<div class="clear"></div>
<!-- phan tiep theo ben duoi admin img -->
<div id="menu_mid">

<div class="slidingbar_line">

    <div class="slidingbar_line_font">{phrase var='theme.menu'} </div>
    <div class="slidingbar_line_img1"></div>


</div>

<!--cuc chua 3 icon-->

<div class="three_icon_menu">

    {if Phpfox::isUser()}


    <a href="{url link='mail'}" class="sliding_mobile"><!-- sliding_mobile:old class-->

        <div class="sidebar_3icon_mess">
            <div class="sidebar_messages"></div>
            <div class="slidingbar_title_mess">{phrase var='mail.mobile_messages'}</div>
        </div>
    </a>


    <a href="{url link=''}" class="sliding_mobile">
        <div class="sidebar_3icon_db">
            <div class="sidebar_dashboard"></div>
            <div class="slidingbar_title_db">{phrase var='core.dashboard'}</div>
        </div>
    </a>

    <a href="{url link='friend.accept'}" class="sliding_mobile">

        <div class="sidebar_3icon_fr">
            <div class="sidebar_friends"></div>
            <div class="slidingbar_title_fr">{phrase var='profile.friends'}</div>
        </div>
    </a>



    {else}
    <div class="clear"></div>



    <a href="{url link=''}" class="sliding_mobile"> <div class="icon_list">
            <div class="sidebar_login"></div>
            <div class="slidingbar_title">{phrase var='user.login_button'}</div>
        </div>
    </a>
    <div class="clear"></div>
    <a href="{url link='user.register'}" class="sliding_mobile"> <div class="icon_list">
            <div class="sidebar_sign"></div>
            <div class="slidingbar_title">{phrase var='user.sign'}</div>
        </div>
    </a>



    {/if}
    <div class="clear"></div>

</div>
<!-- het cuc 3 icon -->
<div class="slidingbar_line">

    <div class="slidingbar_line_font">{phrase var='admincp.tools'}</div>
    <div class="slidingbar_line_img1"></div>


</div>

<!--lay icon vao vong lap-->

{foreach from=$aMenuIcon <!--key=iKey--> item=aIcon name=icon}
<a href="{$aIcon.link}" class="sliding_mobile">

    <div class="icon_list">

        {if isset($aIcon.total) && $aIcon.total > 0}
        <span class="new">{$aIcon.total}</span>
        {/if}

        <div id="icon_list_image" class="logo_retina">

            <img src="{$aIcon.icon}" alt=""height="37px" width="37px" />

        </div>

        <div class="slidingbar_title" style="margin-left: 0px;">{$aIcon.phrase}</div>
    </div>
</a>

<div class="clear"></div>


{/foreach}
<div class="clear"></div>
<!--het lay icon tu vong lap -->

<!--gach ngang setting-->
{if Phpfox::isUser()}
<div class="slidingbar_line_logout">
    <div class="slidingbar_line_font">{phrase var='mobile.logout'} </div>
    <div class="slidingbar_line_img"></div>
</div>


<div class="clear"></div>

<a href="{url link='user.logout'}" class="sliding_mobile"> <div class="icon_list">
        <div class="sidebar_logout"></div>
        <div class="slidingbar_title_logout">{phrase var='mobile.logout'}</div>
    </div>
</a>
{/if}

<div class="clear"></div>
    <div class="slidingbar_line">
        <div class="slidingbar_line_font">{phrase var='admincp.settings'} </div>
        <div class="slidingbar_line_img"></div>
    </div>
<!--menu logout full-->
<div class="clear"></div>
<a href="{url link='go-to-full-site'}" class="sliding_mobile"> <div class="icon_list">
        <div class="slidingbar_full" ></div>
        <div class="slidingbar_title" style="margin-left: 2px;">{phrase var='mobile.full_site'}</div>

    </div>
</a>
    <div class="clear"></div>

    <a href="{url link='user.setting'}" class="sliding_mobile"> <div class="icon_list">
        <div class="slidingbar_language"></div>
        <div class="slidingbar_title"  style="margin-left: 2px;" >{$sLocaleName}</div>
    </div>
</a>

<div class="clear"></div>
<div class="slidingbar_bot"></div>
</div>

<div class="clear"></div>
