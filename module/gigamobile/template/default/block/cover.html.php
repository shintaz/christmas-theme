<?php
/**
 * [PHPFOX_HEADER]
 *
 * @copyright		[PHPFOX_COPYRIGHT]
 * @author  		Raymond Benc
 * @package  		Module_Profile
 * @version 		$Id: logo.html.php 4914 2012-10-22 07:52:17Z Raymond_Benc $
 */

defined('PHPFOX') or exit('NO DICE!');

?>
{if $aCoverUser}
{literal}
<style type="text/css">
    #mobile_profile_photo_image{
        position: relative;
        min-height: 80px;
        padding-bottom: 15px;
    }
    .mobile_profile_info {
        color: #737373;
        font-size: 11px;
        margin-left: 26px;
        margin-top: 36px;
    }
    #mobile_profile_photo_image {
        position: absolute;
        top: -69px !important;
        left: 0px;
        width: 100px !important;
        height: 100px !important;
        background: #ffffff;
        margin: 16px;
        padding: 0px;
        border: #ffffff 2px solid;
        top: -18px;
        -webkit-box-shadow: 0px 1px 0px rgba(50, 50, 50, 0.49);
        -moz-box-shadow: 0px 1px 0px rgba(50, 50, 50, 0.49);
        box-shadow: 2px 1px 6px rgba(50, 50, 50, 0.49);
    }
    #mobile_profile_header {
        background: #ffffff;
        color: #323232;
        padding-top: 60px;
        margin-bottom: 15px;
        position: relative;
        border-top: none !important;
        -webkit-box-shadow: 0px 1px 3px rgba(50, 50, 50, 0.49);
        -moz-box-shadow: 0px 1px 3px rgba(50, 50, 50, 0.49);
        box-shadow: 0px 1px 3px rgba(50, 50, 50, 0.49);
    }
    #site_content {
        padding-top: 0px !important;}
</style>
{/literal}
{else}
<div class="cover_photo_link">
    <a href="{permalink module='photo' id=$aCoverPhoto.photo_id title=$aCoverPhoto.title}userid_{$aCoverPhoto.user_id}/" class="thickbox photo_holder_image cover_photo_link" rel="{$aCoverPhoto.photo_id}">
        {img id='js_photo_cover_position' server_id=$aCoverPhoto.server_id path='photo.url_photo' file=$aCoverPhoto.destination suffix='_1024' title=$aCoverPhoto.title style='position:absolute; width: 100%; min-width:386px;min-height:300px' data-position=$sLogoPosition}
    </a>
</div>
{literal}
<script type="text/javascript" language="javascript">
    $Behavior.positionCoverPhoto = function() {
        $('.cover_photo_link img').load(function() {
            $(this).width(980);
            var naturalHeight = $(this).height();
            var position = $(this).data('position');

            // set image to full width
            $(this).width('100%');
            // get new height
            var height = $(this).height();
            var percent = height / naturalHeight;

            var position = percent * position;
            $(this).css('top', position + 'px');

        });
    }
</script>

<style type="text/css">
    #mobile_profile_photo_image{
        position: absolute;
        top:-18px;
    }
    .mobile_profile_name{
        top:3px;
    }
    #mobile_profile_header{
        border-top: 1px #cacaca solid;
        min-height: 90px;
    }
</style>
{/literal}
{/if}