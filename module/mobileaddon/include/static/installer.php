<?php
if (!class_exists('BrodevThemesupporterInstaller')) {
    class BrodevThemesupporterInstaller
    {

        var $bNoInstall = false;

        function install()
        {
            if ($this->bNoInstall) {
                return false;
            }

            $oDb = Phpfox::getLib('phpfox.database');
            $aSql = array(
                "DROP TABLE IF EXISTS `" . Phpfox::getT('brodev_block_introduction') . "`",
                "CREATE TABLE `" . Phpfox::getT('brodev_block_introduction') . "` (
                   `id` INT NOT NULL AUTO_INCREMENT PRIMARY KEY ,
                    `title` VARCHAR( 250 ) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL ,
                    `button` VARCHAR( 100 ) NOT NULL ,
                    `link` VARCHAR( 150 ) NOT NULL ,
                    `detail` TEXT CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL ,
                    `ordering` INT NOT NULL ,
                    `image` VARCHAR( 150 ) NOT NULL ,
                    `is_show` BOOLEAN NOT NULL
                )"
            );
            foreach ($aSql as $sSql) {
                $oDb->query($sSql);
            }
        }

        function upgrade()
        {

        }

        function uninstall()
        {
            $oDb = Phpfox::getLib('phpfox.database');
            $aSql = array(
                'DROP TABLE IF EXISTS `' . Phpfox::getT('brodev_block_introduction') . '`',
            );
            foreach ($aSql as $sSql) {
                $oDb->query($sSql);
            }
        }
    }
}

/* BRODEV LICENSE PROTECTOR CODE */

$oRequest = Phpfox::getLib('phpfox.request');
if ($oRequest->get('upgrade')) {
    $sType = 'upgrade';
} else if ($oRequest->get('delete')) {
    $sType = 'uninstall';
} else if ($oRequest->get('install')) {
    $sType = 'install';
}
$oInstaller = new BrodevThemesupporterInstaller();
$oInstaller->{$sType}();