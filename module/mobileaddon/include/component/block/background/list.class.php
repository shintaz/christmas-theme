<?php
class Mobileaddon_Component_Block_Background_List extends Phpfox_Component {
    public function process() {
        $aImages = Phpfox::getService('mobileaddon.background')->getList();
        $this->template()
            ->assign(array(
            'aImages' => $aImages
        ));
    }
}
