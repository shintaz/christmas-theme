<?php
/**
 * Created by JetBrains PhpStorm.
 * User: Hz
 * Date: 11/15/12
 * Time: 2:32 PM
 * To change this template use File | Settings | File Templates.
 */
class Mobileaddon_Component_Controller_Admincp_Settheme_Index extends Phpfox_Component
{
    public function process()
    {
        $aThemes = Phpfox::getService('mobileaddon.theme')->getThemes();
        $aStyles = Phpfox::getService('mobileaddon.theme')->getStyles();

        $aDefaultStyle = Phpfox::getService('mobileaddon.theme')
            ->getDefaultMobileStyle();

        $this->template()->setBreadcrumb(Phpfox::getPhrase('mobileaddon.mobile_theme'))
            ->assign(array(
                'aThemes' => $aThemes,
                'aStyles' => $aStyles,
                'aDefaultStyle' => $aDefaultStyle
            )
        );

        if ($aVals = $this->request()->getArray('val')) {
            if (Phpfox::getService('mobileaddon.theme')->saveMobileTheme($aVals)) {
                $this->url()->send(
                    'admincp.mobileaddon.settheme', array(), 'Updated!'
                );
            }
        }
    }
}
