<?php
class Mobileaddon_Component_Controller_Admincp_Background_Edit extends Phpfox_Component {
    public function process() {
        $iId = $this->request()->getInt('id');
        if ($aVals = $this->request()->getArray('val')) {
            if (Phpfox::getService('mobileaddon.background.process')->update($aVals, $aVals['id'])) {
                $this->url()->send('admincp.mobileaddon.background', array(), 'Update Successfully');
            }
        }
        if ($iId == 0) {
            $this->url()->send('admincp.mobileaddon.background', array(), 'item not found');
        }
        $aImage = Phpfox::getService('mobileaddon.background')->getForEdit($iId);
        if (empty($aImage)) {
            $this->url()->send('admincp.mobileaddon.background', array(), 'item not found');
        }
        $this->template()
            ->setBreadcrumb('Manager Background Images')
            ->assign('aForms', $aImage);
        return true;
    }
}
