<?php
class Mobileaddon_Component_Controller_Admincp_Background_Index extends Phpfox_Component {
    public function process() {
        //bread crumb
        $this->template()
            ->setBreadcrumb(Phpfox::getPhrase('mobileaddon.manager_background_images'));
        //add new
        if ($aVals = $this->request()->getArray('val')) {
            if (Phpfox::getService('mobileaddon.background.process')->add($aVals)) {
                $this->url()->send('admincp.mobileaddon.background', array(), Phpfox::getPhrase('mobileaddon.add_new_image_successfully'));
            }
        }
        // delete
        if ($iDelete = $this->request()->get('delete')) {
            if (Phpfox::getService('mobileaddon.background.process')->delete($iDelete)) {
                $this->url()->send('admincp.mobileaddon.background', array(), Phpfox::getPhrase('mobileaddon.delete_image_successfully'));
            }
        }
    }
}

