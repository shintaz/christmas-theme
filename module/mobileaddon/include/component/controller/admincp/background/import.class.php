<?php
class Mobileaddon_Component_Controller_Admincp_Background_Import extends Phpfox_Component {
    public function process() {
        $sLink = "admincp.mobileaddon.background.import";
        if ($this->request()->get('import') ) {
            if (Phpfox::getService('mobileaddon.background')->importSample()) {
                $this->url()->send($sLink,array(),Phpfox::getPhrase('mobileaddon.import_sample_background_successfully'));
            }

        }
        $this->template()
            ->setBreadcrumb(Phpfox::getPhrase('mobileaddon.import_background_sample'));
    }
}
