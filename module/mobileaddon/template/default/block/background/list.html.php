<div class="table_header">
    {phrase var='mobileaddon.manager_background_images'}
</div>
<table cellpadding="0" cellspacing="0">
    <tr>
        <th style="width: 150px">{phrase var='mobileaddon.title'}</th>
        <th style="width: 120px">{phrase var='mobileaddon.image'}</th>
        <th class="t_center" style="width:60px;">{phrase var='mobileaddon.active'}</th>
        <th class="t_center" style="width:60px;">{phrase var='mobileaddon.action'}</th>
    </tr>
    {foreach from=$aImages item=aImage}
    <tr>
        <td>{$aImage.title}</td>
        <td>{img path='mobileaddon.image_url' file=$aImage.image suffix='_100_square'}</td>
        <td>{if $aImage.active}  {phrase var='admincp.yes'} {else} {phrase var='admincp.no'} {/if}</td>
        <td>
            <a href="{url link='admincp.mobileaddon.background.edit' id=$aImage.id}">{phrase var='mobileaddon.edit'}</a>
            <a href="{url link='admincp.mobileaddon.background' delete=$aImage.id}" class="sJsConfirm">{phrase var='mobileaddon.delete'}</a>
        </td>
    </tr>
    {foreachelse}

    {/foreach}
</table>
