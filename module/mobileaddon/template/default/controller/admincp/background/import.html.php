<div class="table_header">
    {phrase var='mobileaddon.import_background_sample'}
</div>
<form action="{url link='admincp.mobileaddon.background.import'}" method="post">
    <div class="table_clear">
        <div>{phrase var='mobileaddon.you_should_import_one_time'}</div>
        <input type="submit" value="{phrase var='mobileaddon.import_background_sample'}" class="button sJsConfirm"  name="import">
    </div>
</form>